<?php


function commerce_ecpay_invoice_rules_action_info() {
  return array(
    'Send_invoice_to_ecpay' => array(
      'label' => '傳送發票資料到綠界科技',
      'parameter' => array(
        'commerce-order' => array(
            'type' => 'commerce_order',
            'label' => t('Order in checkout'),
        ),
      ),
      'group' => t('Commerce Order'),
      'base' => 'rules_action_send_invoice_ecpay',
      'callbacks' => array(
        'execute' => 'rules_send_ecpay_invoice'
      ),
    ),
  );
}
/**
 *  Action callback:
 *  發送發票資訊到綠界
 */
function rules_send_ecpay_invoice($order){

    $order_id = $order->order_id;
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'ecpay_invoice')
          ->entityCondition('bundle', 'ecpay_invoice')
          ->fieldCondition('field_status', 'value', 'initial', '=')
          ->fieldCondition('field_commerce_order', 'target_id', $order_id)
          ->addMetaData('account', user_load(1));
    $result = $query->execute();

    if (isset($result['ecpay_invoice'])) {
      $invoiceItems = array_keys($result['ecpay_invoice']);
      $invoiceWrapper = entity_metadata_wrapper('ecpay_invoice',$invoiceItems[0]);
    }
    else{
      drupal_set_message("沒有發票資料，無法送出",'error');
      watchdog('ECPayInvoice','沒有發票資料，無法送出',array(),'WATCHDOG_ERROR');
      return;
    }

    try
    {
      $sMsg = '' ;
      $ecpay_invoice = new EcpayInvoice ;

      // 2.寫入基本介接參數
      $ecpay_invoice->Invoice_Method = 'INVOICE' ; //立即開立發票
      if(variable_get('ecpayInvoiceStatus','dev')=='dev'){
        $ecpay_invoice->Invoice_Url = ECPAY_TEST_SERVICE_URL ;
      }
      elseif(variable_get('ecpayInvoiceStatus','dev')=='production'){
        $ecpay_invoice->Invoice_Url = ECPAY_SERVICE_URL ;
      }
      $ecpay_invoice->MerchantID = variable_get('ecpayInvoiceMerchantID','2000132') ;
      $ecpay_invoice->HashKey = variable_get('ecpayInvoiceHashKey','ejCk326UnaZWKisg');
      $ecpay_invoice->HashIV = variable_get('ecpayInvoiceHashIV','q9jcZX8Ib9LM8wYk');

      // 3.寫入發票相關資訊
      $aItems	= array();
      // 商品資訊
      $ItemJsonData = $invoiceWrapper->field_itemjsondata->value();
      $ItemData = drupal_json_decode($ItemJsonData);
      foreach($ItemData as $value){
        array_push($ecpay_invoice->Send['Items'], array('ItemName' => $value['ItemName'], 'ItemCount' => $value['ItemCount'], 'ItemWord' => $value['ItemWord'], 'ItemPrice' => $value['ItemPrice'], 'ItemTaxType' => $value['ItemTaxType'], 'ItemAmount' => $value['ItemAmount'], 'ItemRemark' => $value['ItemRemark']  )) ;
      }

      $RelateNumber = 'ECPAY'. date('YmdHis') . $invoiceWrapper->id->value() ; // 產生測試用自訂訂單編號
      $ecpay_invoice->Send['RelateNumber'] = $RelateNumber ;
      $ecpay_invoice->Send['CustomerID'] = $invoiceWrapper->field_customerid->value()->uid;
      $ecpay_invoice->Send['CustomerIdentifier'] = $invoiceWrapper->field_customeridentifier->value();
      $ecpay_invoice->Send['CustomerName'] = $invoiceWrapper->field_customername->value();
      $ecpay_invoice->Send['CustomerAddr'] = $invoiceWrapper->field_customeraddr->value() ;
      $ecpay_invoice->Send['CustomerPhone'] = $invoiceWrapper->field_customerphone->value() ;
      $ecpay_invoice->Send['CustomerEmail'] = $invoiceWrapper->field_customeremail->value();
      $ecpay_invoice->Send['ClearanceMark'] = $invoiceWrapper->field_clearancemark->value() ;
      $ecpay_invoice->Send['Print'] = $invoiceWrapper->field_print->value() ;
      $ecpay_invoice->Send['Donation'] = $invoiceWrapper->field_donation->value() ;
      $ecpay_invoice->Send['LoveCode'] = $invoiceWrapper->field_lovecode->value() ;
      $ecpay_invoice->Send['CarruerType'] = $invoiceWrapper->field_carruertype->value() ;
      $ecpay_invoice->Send['CarruerNum'] = $invoiceWrapper->field_carruernum->value() ;
      $ecpay_invoice->Send['TaxType'] = $invoiceWrapper->field_taxtype->value();
      $ecpay_invoice->Send['SalesAmount'] = $invoiceWrapper->field_salesamount->value();
      $ecpay_invoice->Send['InvoiceRemark'] =  $invoiceWrapper->field_invoiceremark->value();
      $ecpay_invoice->Send['InvType'] = $invoiceWrapper->field_invtype->value() ;
      $ecpay_invoice->Send['vat'] = $invoiceWrapper->field_vat->value() ;
       // 4.送出
      $aReturn_Info = $ecpay_invoice->Check_Out();

      // 5.返回
      $invoiceWrapper->field_relatenumber->set($RelateNumber);
      $invoiceWrapper->field_invoicenumber->set($aReturn_Info['InvoiceNumber']);
      $invoiceWrapper->field_invoicedate->set(strtotime($aReturn_Info['InvoiceDate']));
      $invoiceWrapper->field_randomnumber->set($aReturn_Info['RandomNumber']);
      $invoiceWrapper->field_rtncode->set($aReturn_Info['RtnCode']);
      $invoiceWrapper->field_rtnmsg->set($aReturn_Info['RtnMsg']);
      $invoiceWrapper->field_checkmacvalue->set($aReturn_Info['CheckMacValue']);
      if($aReturn_Info['RtnCode']==1){
        $invoiceWrapper->field_status->set('succeed');
      }
      else{
        $invoiceWrapper->field_status->set('failed');
      }
      $invoiceWrapper->save();
    }
    catch (Exception $e)
    {
      // 例外錯誤處理。
      $sMsg = $e->getMessage();
      $invoiceWrapper->field_rtnmsg->set($sMsg);
      $invoiceWrapper->field_status->set('failed');
      $invoiceWrapper->save();
    }
}