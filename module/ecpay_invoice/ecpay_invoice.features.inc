<?php

/**
 * @file
 * ecpay_invoice.features.inc
 */

/**
 * Implements hook_views_api().
 */
function ecpay_invoice_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_eck_bundle_info().
 */
function ecpay_invoice_eck_bundle_info() {
  $items = array(
    'ecpay_invoice_ecpay_invoice' => array(
      'machine_name' => 'ecpay_invoice_ecpay_invoice',
      'entity_type' => 'ecpay_invoice',
      'name' => 'ecpay_invoice',
      'label' => 'ECPay Invoice',
      'config' => array(
        'managed_properties' => array(
          'uid' => 'uid',
          'created' => 'created',
          'changed' => 'changed',
          'title' => 0,
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function ecpay_invoice_eck_entity_type_info() {
  $items = array(
    'ecpay_invoice' => array(
      'name' => 'ecpay_invoice',
      'label' => 'ECPay Invoice',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
        'uid' => array(
          'label' => 'Author',
          'type' => 'integer',
          'behavior' => 'author',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
      ),
    ),
  );
  return $items;
}
