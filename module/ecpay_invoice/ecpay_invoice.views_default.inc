<?php

/**
 * @file
 * ecpay_invoice.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ecpay_invoice_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'ecpay_invoice_list';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'eck_ecpay_invoice';
  $view->human_name = '綠界電子發票';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = '綠界電子發票';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'view ecpay invoice';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = '重設';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '40';
  $handler->display->display_options['pager']['options']['tags']['first'] = '« 第一頁';
  $handler->display->display_options['pager']['options']['tags']['previous'] = '‹ 上一頁';
  $handler->display->display_options['pager']['options']['tags']['next'] = '下一頁 ›';
  $handler->display->display_options['pager']['options']['tags']['last'] = '最後一頁 »';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'field_status' => 'field_status',
    'field_print' => 'field_print',
    'field_invtype' => 'field_invtype',
    'field_customername' => 'field_customername',
    'field_customeraddr' => 'field_customername',
    'field_customerphone' => 'field_customername',
    'field_customerid' => 'field_customerid',
    'field_customeremail' => 'field_customeremail',
    'field_donation' => 'field_donation',
    'field_lovecode' => 'field_lovecode',
    'field_invoiceremark' => 'field_invoiceremark',
    'field_salesamount' => 'field_salesamount',
    'field_invoicenumber' => 'field_invoicenumber',
    'field_invoicedate' => 'field_invoicedate',
    'field_customeridentifier' => 'field_customeridentifier',
    'field_taxtype' => 'field_taxtype',
    'field_carruertype' => 'field_carruertype',
    'field_clearancemark' => 'field_clearancemark',
    'field_rtncode' => 'field_rtncode',
    'field_rtnmsg' => 'field_rtnmsg',
    'view_link' => 'view_link',
    'view_order' => 'view_link',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_status' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_print' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_invtype' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_customername' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '</br>',
      'empty_column' => 0,
    ),
    'field_customeraddr' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_customerphone' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_customerid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_customeremail' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_donation' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_lovecode' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_invoiceremark' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_salesamount' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_invoicenumber' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_invoicedate' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_customeridentifier' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_taxtype' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_carruertype' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_clearancemark' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_rtncode' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_rtnmsg' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'view_link' => array(
      'align' => '',
      'separator' => '</br>',
      'empty_column' => 0,
    ),
    'view_order' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_commerce_order_target_id']['id'] = 'field_commerce_order_target_id';
  $handler->display->display_options['relationships']['field_commerce_order_target_id']['table'] = 'field_data_field_commerce_order';
  $handler->display->display_options['relationships']['field_commerce_order_target_id']['field'] = 'field_commerce_order_target_id';
  /* 欄位: ECPay Invoice: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'eck_ecpay_invoice';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '標題';
  /* 欄位: ECPay Invoice: status */
  $handler->display->display_options['fields']['field_status']['id'] = 'field_status';
  $handler->display->display_options['fields']['field_status']['table'] = 'field_data_field_status';
  $handler->display->display_options['fields']['field_status']['field'] = 'field_status';
  $handler->display->display_options['fields']['field_status']['label'] = '狀態';
  /* 欄位: ECPay Invoice: 列印註記 */
  $handler->display->display_options['fields']['field_print']['id'] = 'field_print';
  $handler->display->display_options['fields']['field_print']['table'] = 'field_data_field_print';
  $handler->display->display_options['fields']['field_print']['field'] = 'field_print';
  /* 欄位: ECPay Invoice: 字軌類別 */
  $handler->display->display_options['fields']['field_invtype']['id'] = 'field_invtype';
  $handler->display->display_options['fields']['field_invtype']['table'] = 'field_data_field_invtype';
  $handler->display->display_options['fields']['field_invtype']['field'] = 'field_invtype';
  /* 欄位: ECPay Invoice: 客戶名稱 */
  $handler->display->display_options['fields']['field_customername']['id'] = 'field_customername';
  $handler->display->display_options['fields']['field_customername']['table'] = 'field_data_field_customername';
  $handler->display->display_options['fields']['field_customername']['field'] = 'field_customername';
  $handler->display->display_options['fields']['field_customername']['label'] = '客戶資訊';
  /* 欄位: ECPay Invoice: 客戶地址 */
  $handler->display->display_options['fields']['field_customeraddr']['id'] = 'field_customeraddr';
  $handler->display->display_options['fields']['field_customeraddr']['table'] = 'field_data_field_customeraddr';
  $handler->display->display_options['fields']['field_customeraddr']['field'] = 'field_customeraddr';
  /* 欄位: ECPay Invoice: 客戶手機號碼 */
  $handler->display->display_options['fields']['field_customerphone']['id'] = 'field_customerphone';
  $handler->display->display_options['fields']['field_customerphone']['table'] = 'field_data_field_customerphone';
  $handler->display->display_options['fields']['field_customerphone']['field'] = 'field_customerphone';
  /* 欄位: ECPay Invoice: 客戶編號 */
  $handler->display->display_options['fields']['field_customerid_1']['id'] = 'field_customerid_1';
  $handler->display->display_options['fields']['field_customerid_1']['table'] = 'field_data_field_customerid';
  $handler->display->display_options['fields']['field_customerid_1']['field'] = 'field_customerid';
  $handler->display->display_options['fields']['field_customerid_1']['type'] = 'entityreference_entity_id';
  $handler->display->display_options['fields']['field_customerid_1']['settings'] = array(
    'bypass_access' => 0,
    'link' => 0,
  );
  /* 欄位: ECPay Invoice: 客戶電子信箱 */
  $handler->display->display_options['fields']['field_customeremail']['id'] = 'field_customeremail';
  $handler->display->display_options['fields']['field_customeremail']['table'] = 'field_data_field_customeremail';
  $handler->display->display_options['fields']['field_customeremail']['field'] = 'field_customeremail';
  /* 欄位: ECPay Invoice: 捐款註記 */
  $handler->display->display_options['fields']['field_donation']['id'] = 'field_donation';
  $handler->display->display_options['fields']['field_donation']['table'] = 'field_data_field_donation';
  $handler->display->display_options['fields']['field_donation']['field'] = 'field_donation';
  /* 欄位: ECPay Invoice: 捐贈碼 */
  $handler->display->display_options['fields']['field_lovecode']['id'] = 'field_lovecode';
  $handler->display->display_options['fields']['field_lovecode']['table'] = 'field_data_field_lovecode';
  $handler->display->display_options['fields']['field_lovecode']['field'] = 'field_lovecode';
  /* 欄位: ECPay Invoice: 發票總金額 (含稅) */
  $handler->display->display_options['fields']['field_salesamount']['id'] = 'field_salesamount';
  $handler->display->display_options['fields']['field_salesamount']['table'] = 'field_data_field_salesamount';
  $handler->display->display_options['fields']['field_salesamount']['field'] = 'field_salesamount';
  $handler->display->display_options['fields']['field_salesamount']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* 欄位: ECPay Invoice: 發票號碼 */
  $handler->display->display_options['fields']['field_invoicenumber']['id'] = 'field_invoicenumber';
  $handler->display->display_options['fields']['field_invoicenumber']['table'] = 'field_data_field_invoicenumber';
  $handler->display->display_options['fields']['field_invoicenumber']['field'] = 'field_invoicenumber';
  /* 欄位: ECPay Invoice: 發票開立時間 */
  $handler->display->display_options['fields']['field_invoicedate']['id'] = 'field_invoicedate';
  $handler->display->display_options['fields']['field_invoicedate']['table'] = 'field_data_field_invoicedate';
  $handler->display->display_options['fields']['field_invoicedate']['field'] = 'field_invoicedate';
  $handler->display->display_options['fields']['field_invoicedate']['settings'] = array(
    'format_type' => 'custom',
    'custom_date_format' => 'Y-m-d H:i:s',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* 欄位: ECPay Invoice: 統一編號 */
  $handler->display->display_options['fields']['field_customeridentifier']['id'] = 'field_customeridentifier';
  $handler->display->display_options['fields']['field_customeridentifier']['table'] = 'field_data_field_customeridentifier';
  $handler->display->display_options['fields']['field_customeridentifier']['field'] = 'field_customeridentifier';
  /* 欄位: ECPay Invoice: 課稅類別 */
  $handler->display->display_options['fields']['field_taxtype']['id'] = 'field_taxtype';
  $handler->display->display_options['fields']['field_taxtype']['table'] = 'field_data_field_taxtype';
  $handler->display->display_options['fields']['field_taxtype']['field'] = 'field_taxtype';
  /* 欄位: ECPay Invoice: 載具類別 */
  $handler->display->display_options['fields']['field_carruertype']['id'] = 'field_carruertype';
  $handler->display->display_options['fields']['field_carruertype']['table'] = 'field_data_field_carruertype';
  $handler->display->display_options['fields']['field_carruertype']['field'] = 'field_carruertype';
  /* 欄位: ECPay Invoice: 通關方式 */
  $handler->display->display_options['fields']['field_clearancemark']['id'] = 'field_clearancemark';
  $handler->display->display_options['fields']['field_clearancemark']['table'] = 'field_data_field_clearancemark';
  $handler->display->display_options['fields']['field_clearancemark']['field'] = 'field_clearancemark';
  /* 欄位: ECPay Invoice: 回應代碼 */
  $handler->display->display_options['fields']['field_rtncode']['id'] = 'field_rtncode';
  $handler->display->display_options['fields']['field_rtncode']['table'] = 'field_data_field_rtncode';
  $handler->display->display_options['fields']['field_rtncode']['field'] = 'field_rtncode';
  /* 欄位: ECPay Invoice: 回應訊息 */
  $handler->display->display_options['fields']['field_rtnmsg']['id'] = 'field_rtnmsg';
  $handler->display->display_options['fields']['field_rtnmsg']['table'] = 'field_data_field_rtnmsg';
  $handler->display->display_options['fields']['field_rtnmsg']['field'] = 'field_rtnmsg';
  /* 欄位: ECPay Invoice: 連結 */
  $handler->display->display_options['fields']['view_link']['id'] = 'view_link';
  $handler->display->display_options['fields']['view_link']['table'] = 'eck_ecpay_invoice';
  $handler->display->display_options['fields']['view_link']['field'] = 'view_link';
  $handler->display->display_options['fields']['view_link']['label'] = '操作';
  $handler->display->display_options['fields']['view_link']['text'] = '查看發票';
  /* 欄位: Commerce Order: 連結 */
  $handler->display->display_options['fields']['view_order']['id'] = 'view_order';
  $handler->display->display_options['fields']['view_order']['table'] = 'commerce_order';
  $handler->display->display_options['fields']['view_order']['field'] = 'view_order';
  $handler->display->display_options['fields']['view_order']['relationship'] = 'field_commerce_order_target_id';
  $handler->display->display_options['fields']['view_order']['text'] = '觀看訂單';
  /* Sort criterion: ECPay Invoice: Created */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'eck_ecpay_invoice';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: ECPay Invoice: ecpay_invoice type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_ecpay_invoice';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'ecpay_invoice' => 'ecpay_invoice',
  );
  /* Filter criterion: ECPay Invoice: 載具類別 (field_carruertype) */
  $handler->display->display_options['filters']['field_carruertype_value']['id'] = 'field_carruertype_value';
  $handler->display->display_options['filters']['field_carruertype_value']['table'] = 'field_data_field_carruertype';
  $handler->display->display_options['filters']['field_carruertype_value']['field'] = 'field_carruertype_value';
  $handler->display->display_options['filters']['field_carruertype_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_carruertype_value']['expose']['operator_id'] = 'field_carruertype_value_op';
  $handler->display->display_options['filters']['field_carruertype_value']['expose']['label'] = '載具類別';
  $handler->display->display_options['filters']['field_carruertype_value']['expose']['operator'] = 'field_carruertype_value_op';
  $handler->display->display_options['filters']['field_carruertype_value']['expose']['identifier'] = 'field_carruertype_value';
  $handler->display->display_options['filters']['field_carruertype_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  /* Filter criterion: ECPay Invoice: status (field_status) */
  $handler->display->display_options['filters']['field_status_value']['id'] = 'field_status_value';
  $handler->display->display_options['filters']['field_status_value']['table'] = 'field_data_field_status';
  $handler->display->display_options['filters']['field_status_value']['field'] = 'field_status_value';
  $handler->display->display_options['filters']['field_status_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_status_value']['expose']['operator_id'] = 'field_status_value_op';
  $handler->display->display_options['filters']['field_status_value']['expose']['label'] = '發票狀態';
  $handler->display->display_options['filters']['field_status_value']['expose']['operator'] = 'field_status_value_op';
  $handler->display->display_options['filters']['field_status_value']['expose']['identifier'] = 'field_status_value';
  $handler->display->display_options['filters']['field_status_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  /* Filter criterion: ECPay Invoice: 客戶電子信箱 (field_customeremail) */
  $handler->display->display_options['filters']['field_customeremail_email']['id'] = 'field_customeremail_email';
  $handler->display->display_options['filters']['field_customeremail_email']['table'] = 'field_data_field_customeremail';
  $handler->display->display_options['filters']['field_customeremail_email']['field'] = 'field_customeremail_email';
  $handler->display->display_options['filters']['field_customeremail_email']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_customeremail_email']['expose']['operator_id'] = 'field_customeremail_email_op';
  $handler->display->display_options['filters']['field_customeremail_email']['expose']['label'] = '客戶電子信箱';
  $handler->display->display_options['filters']['field_customeremail_email']['expose']['operator'] = 'field_customeremail_email_op';
  $handler->display->display_options['filters']['field_customeremail_email']['expose']['identifier'] = 'field_customeremail_email';
  $handler->display->display_options['filters']['field_customeremail_email']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/commerce/ecpay_inovice';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = '電子發票';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $translatables['ecpay_invoice_list'] = array(
    t('Master'),
    t('綠界電子發票'),
    t('more'),
    t('Apply'),
    t('重設'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« 第一頁'),
    t('‹ 上一頁'),
    t('下一頁 ›'),
    t('最後一頁 »'),
    t('Commerce Order entity referenced from field_commerce_order'),
    t('標題'),
    t('狀態'),
    t('列印註記'),
    t('字軌類別'),
    t('客戶資訊'),
    t('客戶地址'),
    t('客戶手機號碼'),
    t('客戶編號'),
    t('客戶電子信箱'),
    t('捐款註記'),
    t('捐贈碼'),
    t('發票總金額 (含稅)'),
    t('發票號碼'),
    t('發票開立時間'),
    t('統一編號'),
    t('課稅類別'),
    t('載具類別'),
    t('通關方式'),
    t('回應代碼'),
    t('回應訊息'),
    t('操作'),
    t('查看發票'),
    t('連結'),
    t('觀看訂單'),
    t('發票狀態'),
    t('Page'),
  );
  $export['ecpay_invoice_list'] = $view;

  return $export;
}
