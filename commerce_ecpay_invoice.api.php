<?php

/**
 * @file 
 * Hooks provided by the commerce_ecpay_invoice modules
 */

/**
 *  @param array $data
 *  要送去給綠界的基本發票資料，一筆訂單會對應到一筆發票資料，可以在這裡進行資料的修改
 *  以下是$data裡需要具備的參數
 *  CustomerID: 客戶編號
 *  CustomerIdentifier: 統一編號
 *  CustomerName: 客戶名稱
 *  CustomerAddr: 客戶地址
 *  CustomerPhone: 客戶手機號碼
 *  CustomerEmail：客戶電子信箱
 *  ClearanceMark：通關方式
 *  Print：列印註記
 *  Donation：捐贈註記
 *  LoveCode：捐贈碼
 *  CarruerType：載具類別
 *  CarruerNum：載具編號
 *  TaxType：課稅類別
 *  SalesAmount：發票總金額
 *  InvoiceRemark：發票備註
 *  ItemName：商品名稱
 *  ItemCount：商品數量
 *  ItemWord：商品單位
 *  ItemPrice：商品價格
 *  ItemTaxType：商品課稅別
 *  ItemAmount：商品合計
 *  ItemRemark：商品備註說明
 *  InvType：字軌類別
 *  vat：商品單價是否含稅
 */
function hook_commerce_ecpay_invoice_data_alter(&$data, $order){
    // No example.
}