<?php
/**
 * @file 
 * Admin Form with commerce_ecpay_invoice
 */

function commerce_ecpay_invoice_settings_form($form, &$form_state){
    $form['ecpayInvoiceStatus']=array(
    '#type' => 'radios',
    '#title'=> '綠界系統狀態',
    '#description'=> '選擇對應的系統狀態。例如：測試環境、正式環境',
    '#options' => array(
        'dev' => '測試環境',
        'production' => '正式環境',
    ),
    '#default_value' => variable_get('ecpayInvoiceStatus','dev'),
    '#required' => TRUE,
    '#weight'=>0,
  );

  $form['ecpayInvoiceMerchantID'] = array(
    '#type'=>'textfield',
    '#title'=>'MerchantID',
    '#description'=>'綠界提供的MerchantID，測試環境可以使用：2000132',
    '#maxlength' => 255,
    '#size' => 30,
    '#required' => TRUE,
    '#default_value' => variable_get('ecpayInvoiceMerchantID','2000132'),
    '#weight'=>1,
  );

  $form['ecpayInvoiceHashKey'] = array(
    '#type'=>'textfield',
    '#title'=>'HashKey',
    '#description'=>'綠界提供的HashKey，測試環境可以使用：ejCk326UnaZWKisg',
    '#maxlength' => 255,
    '#size' => 30,
    '#required' => TRUE,
    '#default_value' => variable_get('ecpayInvoiceHashKey','ejCk326UnaZWKisg'),
    '#weight'=>2,
  );

  $form['ecpayInvoiceHashIV'] = array(
    '#type'=>'textfield',
    '#title'=>'HashIV',
    '#description'=> '綠界提供的HashIV，測試環境可以使用：q9jcZX8Ib9LM8wYk',
    '#maxlength' => 255,
    '#size' => 30,
    '#required' => TRUE,
    '#default_value' => variable_get('ecpayInvoiceHashIV','q9jcZX8Ib9LM8wYk'),
    '#weight'=>3,
  );

  $form['submit']=array(
    '#type'=>'submit',
    '#weight'=>10,
    '#value'=> '儲存',
  );
  return $form;
}

function commerce_ecpay_invoice_settings_form_submit($form,&$form_state){

  $ecpayInvoiceStatus = $form_state['values']['ecpayInvoiceStatus'];
  $ecpayInvoiceMerchantID = $form_state['values']['ecpayInvoiceMerchantID'];
  $ecpayInvoiceHashKey = $form_state['values']['ecpayInvoiceHashKey'];
  $ecpayInvoiceHashIV = $form_state['values']['ecpayInvoiceHashIV'];

  variable_set('ecpayInvoiceStatus',$ecpayInvoiceStatus);
  variable_set('ecpayInvoiceMerchantID',$ecpayInvoiceMerchantID);
  variable_set('ecpayInvoiceHashKey',$ecpayInvoiceHashKey);
  variable_set('ecpayInvoiceHashIV',$ecpayInvoiceHashIV);

  drupal_set_message(t('Update ECPay Invoice Succeed'));
}