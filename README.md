# Commerce ECPay Invoice 綠界電子發票系統模組
目前持續在開發中，目前只有完成到**立即開立**的功能，捐贈發票、自然人憑證、手機載具等等功能都還有待驗證

## ToDo 
1. 完成捐贈發票、自然人憑證、手機載具等等流程驗證
2. 開立折讓
3. 作廢發票
4. 作廢折讓
5. 查詢發票各項資訊


##適用環境
1. Drupal Commerce

## 安裝方法：
1. 下載模組到 sites/all/modules or sites/all/modules/contrib
2. 從https://github.com/cobenash/ECPayAIO_PHP下載Library，解壓縮以後，將EInvoiceSDK目錄放到sites/all/libraries裡面
3. 將資料夾重新命名為ECPayEInvoiceSDK
4. 啟用模組
5. 在狀態報告裡面確認，ECPayEInvoiceSDK Libraries是否有正常啟用

## 使用方法：
1. 在購物流程中，將會有一個【填寫發票資料】的Pane與發票資料的頁面。
2. 進入admin/commerce/config/checkout/form/pane/invoice_data_pane，可以決定選擇電子發票流程的方法，預設的方法為使用【綠界科技電子發票載具】
3. 在購物流程中，將會自動建立電子發票記錄，記錄其狀態為【初始化】
4. 購物完成後，要將電子發票的資料傳送給綠界，這個流程已經整合到Rules當中，可以依照自己的喜好，加入到Rules裡面。例如：當完成訂單要寄送通知的時候，可以多一個Action。寄送發票資訊給綠界。

## 如何調整送去綠界的數值
可以參考API文件，使用hook_commerce_ecpay_invoice_data_alter，來客製化送到綠界的發票資料

